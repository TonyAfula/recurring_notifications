package com.shalommedia.recurringreminder.helpers;

import android.content.Context;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.shalommedia.recurringreminder.services.reminder.OpenURLWorker;

import java.util.concurrent.TimeUnit;

public class WorkManagerHelper {
    /* Notification frequency in minutes */
    public static final int NOTIFICATION_FREQUENCY_MIN = 15;//

    public  void schedule(Context context) {
        Constraints constraints = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED)
                .build();

        //The minimum repeat interval that can be defined is 15 minutes (same as the JobScheduler API).
        PeriodicWorkRequest saveRequest =
                new PeriodicWorkRequest.Builder(OpenURLWorker.class, NOTIFICATION_FREQUENCY_MIN, TimeUnit.MINUTES)
                        .setConstraints(constraints)
                        .build();

        WorkManager.getInstance(context)
                .enqueue(saveRequest);


    }
}
