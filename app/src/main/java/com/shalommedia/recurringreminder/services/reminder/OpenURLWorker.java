package com.shalommedia.recurringreminder.services.reminder;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.shalommedia.recurringreminder.utils.ChromeTabUtils;
import com.shalommedia.recurringreminder.views.MainActivity;

public class OpenURLWorker extends Worker {
    private String TAG= "OpenURLWorker";
    private Context mContext;

    public OpenURLWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);

        mContext = context;
    }
    @Override
    public Result doWork() {
        Log.i(TAG, "doWork....");

        Intent mainIntent = new Intent(mContext, MainActivity.class);
        mainIntent.putExtra(ChromeTabUtils.URL_EXTRA, ChromeTabUtils.URL_2);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(mainIntent);


        // Indicate whether the task finished successfully with the Result
        return Result.success();
    }


}
