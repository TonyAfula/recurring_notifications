package com.shalommedia.recurringreminder.services.injection;

import com.shalommedia.recurringreminder.services.alarm.AlarmService;
import com.shalommedia.recurringreminder.services.alarm.IAlarmService;
import com.shalommedia.recurringreminder.services.log.ILog;
import com.shalommedia.recurringreminder.services.log.Log;
import com.shalommedia.recurringreminder.services.reminder.IReminderService;
import com.shalommedia.recurringreminder.services.reminder.ReminderService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by batra on 2017-05-09.
 */
@Module
public class SingletonsModule {
    @Provides @Singleton static ILog provideLog() { return new Log(); }
    @Provides @Singleton static IAlarmService provideAlarmService() { return new AlarmService(); }
    @Provides @Singleton static IReminderService provideReminderService(IAlarmService alarm, ILog log) {
        return new ReminderService(alarm,  log);
    }

}
