package com.shalommedia.recurringreminder.services.injection;

import com.shalommedia.recurringreminder.android.AlarmTriggerReceiver;
import com.shalommedia.recurringreminder.android.BootCompletedReceiver;
import com.shalommedia.recurringreminder.android.PackageReplacedReceiver;
import com.shalommedia.recurringreminder.android.RecurringReminderApplication;
import com.shalommedia.recurringreminder.services.alarm.AlarmService;
import com.shalommedia.recurringreminder.views.LogActivity;
import com.shalommedia.recurringreminder.views.MainActivity;
import com.shalommedia.recurringreminder.views.adapters.LogsAdapter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by batra on 2017-05-09.
 */
@Singleton @Component(modules = SingletonsModule.class)
public interface SingletonsComponent {
    void inject(AlarmService service);


    void inject(MainActivity activity);
    void inject(LogActivity activity);



    void inject(LogsAdapter adapter);

    void inject(RecurringReminderApplication application);

    void inject(BootCompletedReceiver receiver);
    void inject(PackageReplacedReceiver receiver);
    void inject(AlarmTriggerReceiver receiver);
}
