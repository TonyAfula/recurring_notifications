package com.shalommedia.recurringreminder.views;

import android.os.Bundle;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuItem;

import com.shalommedia.recurringreminder.R;
import com.shalommedia.recurringreminder.views.adapters.LogsAdapter;
import com.shalommedia.recurringreminder.views.adapters.ReloadDoneCallback;
import com.shalommedia.recurringreminder.views.components.EmptyableRecyclerView;
import io.realm.Sort;

import static com.shalommedia.recurringreminder.services.injection.SingletonsInjector.getInjector;

public class LogActivity extends AppCompatActivity {

    private EmptyableRecyclerView mRecycler;
    private LogsAdapter mAdapter;
    private SwipeRefreshLayout mRefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getInjector().inject(this);

        mRecycler = (EmptyableRecyclerView) findViewById(R.id.logs_recycler);
        mAdapter = new LogsAdapter(this);
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecycler.setEmptyView(findViewById(R.id.logs_empty_view));

        mRefresh = (SwipeRefreshLayout) findViewById(R.id.logs_refresh);
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapter.reload(new ReloadDoneCallback() {
                    @Override
                    public void onReloadDone() {
                        mRefresh.setRefreshing(false);
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.log_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.log_menu_sort_asc:
                mAdapter.sort(Sort.ASCENDING);
                return true;
            case R.id.log_menu_sort_des:
                mAdapter.sort(Sort.DESCENDING);
                return true;
            case R.id.log_menu_clear:
                mAdapter.clear();
                return true;
            case R.id.log_menu_start_floater:

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
