package com.shalommedia.recurringreminder.views.callbacks;

import androidx.appcompat.view.ActionMode;

/**
 * Activity that hosts the contextual action bar
 */

public interface IMainCabActivity {
    void startCab(ActionMode.Callback callback);
    void onCabStopped();
}
