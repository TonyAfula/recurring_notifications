package com.shalommedia.recurringreminder.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

import com.shalommedia.recurringreminder.services.log.ILog;
import com.shalommedia.recurringreminder.services.reminder.IReminderService;

import static com.shalommedia.recurringreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Listen for package changes e.g. updates.
 */
public class PackageReplacedReceiver extends BroadcastReceiver {
    @Inject IReminderService mReminderService;
    @Inject ILog mLog;

    public PackageReplacedReceiver() {
        getInjector().inject(this);
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.MY_PACKAGE_REPLACED")) {
            mLog.log("Package replaced.");
            mReminderService.resetAlarms(context);
        }
    }
}
