package com.shalommedia.recurringreminder.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import javax.inject.Inject;

import com.shalommedia.recurringreminder.helpers.WorkManagerHelper;
import com.shalommedia.recurringreminder.services.reminder.IReminderService;

import static com.shalommedia.recurringreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Created by batra on 2017-06-07.
 */

public class BootCompletedReceiver extends BroadcastReceiver {
    @Inject IReminderService mReminderService;
    public BootCompletedReceiver() {
        getInjector().inject(this);
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            mReminderService.resetAlarms(context);

//            WorkManagerHelper workManagerHelper =new WorkManagerHelper();
//            workManagerHelper.schedule(context);
        }
    }
}
