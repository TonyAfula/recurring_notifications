package com.shalommedia.recurringreminder.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import javax.inject.Inject;

import com.shalommedia.recurringreminder.services.alarm.IAlarmService;
import com.shalommedia.recurringreminder.services.reminder.IReminderService;

import static com.shalommedia.recurringreminder.services.injection.SingletonsInjector.getInjector;

public class AlarmTriggerReceiver extends BroadcastReceiver {

    @Inject IReminderService mReminderService;
    @Inject IAlarmService mAlarmService;

    @Override
    public void onReceive(Context context, Intent intent) {
        getInjector().inject(this);
        if(intent.getAction().equals(mAlarmService.getConfigs().getTriggerAction(context))
           && intent.getExtras() != null) {
            String triggeredId = intent.getExtras().getString(mAlarmService.getConfigs().getIdKey());
            mReminderService.onReminderTriggered(context, triggeredId);
            Log.d("AlarmTriggerReceiver", "AlarmTriggerReceiver finished");
        }
    }
}
