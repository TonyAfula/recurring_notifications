package com.shalommedia.recurringreminder.android;

import android.app.Application;

import io.realm.Realm;

import static com.shalommedia.recurringreminder.services.injection.SingletonsInjector.getInjector;

/**
 * Created by batra on 2017-05-03.
 */

public class RecurringReminderApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        getInjector().inject(this);
    }


}
