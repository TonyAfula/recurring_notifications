package com.shalommedia.recurringreminder.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PrefUtils {

    /** pref key indicating alarm trigger time */
    public static final String PREF_ALARM_TRIGGER_TIME = "pref_alarm_trigger_time";

    public static Long getPrefAlarmTriggerTime(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getLong(PREF_ALARM_TRIGGER_TIME, System.currentTimeMillis());
    }

    public static void setPrefAlarmTriggerTime(final Context context, final long triggerTime) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putLong(PREF_ALARM_TRIGGER_TIME, triggerTime).commit();


    }
}
