
### About
Recurring notifications is an Android Application that opens URL #1 in custom tab when the user opens the app and
 URL #2 in custom tab everyday minute using AlarmManager

#### Prerequisites
Before running this application, ensure you have [Android Studio IDE](https://developer.android.com/studio) installed.



#### Installation
1. Import the project in Android Studio IDE using **File->New->Import Project Menu**.


2. Run the application using either an emulator or android device using 'Run' command.
   [Read more...](https://developer.android.com/training/basics/firstapp/running-app)

### Modifying the source code
1. To change the time of notification change **NOTIFICATION_FREQUENCY_MIN** value in
    com.shalommedia.recurringreminder.views.MainActivity to a value in minutes.

    ```
      /* Notification frequency in minutes */
      public static final int NOTIFICATION_FREQUENCY_MIN = 1;
    ```


2. To change the urls change the value of the following fields
  in com.shalommedia.recurringreminder.utils.ChromeTabUtils class.

    ```
       public static final String URL_1= "https://google.com#url1";
       public static final String URL_2= "https://yahoo.com#url2";
    ```